## Guide
 + [How to Package Your LATEX Package](https://www.ee.iitb.ac.in/~trivedi/LatexHelp/Latex%20Manual/dtxtut.pdf)
 + [AMD Brand Guidelines](https://www.amd.com/system/files/2017-06/AMD_BrandGuidelines_Original_34751.pdf)
 + [AMD Guide Browse Asserts](https://library.amd.com/guidelines/guide/8ef0efa9-9c0e-4a68-ae4b-41e3340fda32)

## Template
 + [Metropolis](https://github.com/matze/mtheme)
 + [fibeamer](https://gitlab.fi.muni.cz/external_relations/document_templates/fibeamer)
 + [THU-Beamer-Theme](https://github.com/tuna/THU-Beamer-Theme)
 + [BIT_school_Template](https://github.com/CharlieLeee/My_Beamer_Template)
 + [liu-beamer](https://github.com/Rovanion/liu-beamer)
